To send the token in a request, you can do it by sending an attribute api_token in the payload or as a bearer token in the request headers in the form of

Authorisation: Bearer <api_token>
