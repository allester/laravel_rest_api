<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse as JsonResponse;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }

    public function testsRegistersSuccessfully()
    {
        $payload = [
            'name' => 'John',
            'email' => 'john@example.com',
            'password' => 'example123',
            'password_confirmation' => 'example123',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id' => true,
                    'name' => true,
                    'email' => true,
                    'created_at' => true,
                    'updated_at' => true,
                    'api_token' => true,
                ]
            ]);
    }

    public function testsRequiresPasswordEmailAndName()
    {
        $this->json('post', '/api/register')
            ->assertStatus(422)
            ->seeJsonSubset([[
                'name' => 'The name field is required.',
                'email' => 'The email field is required',
                'password' => 'The password field is required',
            ]]);
    }

    public function testsRequirePasswordConfirmation()
    {
        $payload = [
            'name' => 'John',
            'email' => 'john@example.com',
            'password' => 'example123',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(422)
            ->assertEquals([[
                'password' => 'The password confirmation does not match.',
            ]]);
    }
}
