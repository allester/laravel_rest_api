<?php

use Illuminate\Http\Request;
use App\Article;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Access the current user
// Auth::guard('api')->user();
// Auth::guard('api')->check();
// Auth::guard('api')->id();

// Route::get('articles', 'ArticlesController@index');
// Route::get('articles/{id}', 'ArticlesController@show');
// Route::post('articles', 'ArticlesController@store');
// Route::put('articles/{id}', 'ArticlesController@update');
// Route::delete('articles/{id}', 'ArticlesController@destroy');


// Implicit Route Model Binding

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('articles', 'ArticlesController@index');
    Route::get('articles/{article}', 'ArticlesController@show');
    Route::post('articles', 'ArticlesController@store');
    Route::put('articles/{article}', 'ArticlesController@update');
    Route::delete('articles/{article}', 'ArticlesController@destroy');
});

Route::post('register', 'Auth\RegisterController@register');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout');
