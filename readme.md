## Simple Laravel REST API

Laravel REST API built with simple API token authentication and a few unit tests.

### Models Used

_User Model_, 
_Article Model_

### Tests

_RegisterTest_,
_LoginTest_,
_LogoutTest_,
_ArticleTest_
